package com.phunware.test.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.phunware.test.R;
import com.phunware.test.fragment.VenueDetailFragment;
import com.phunware.test.model.Venue;

/**
 * An activity representing a single Venue detail screen. This
 * activity is only used on handset devices.
 */
public class VenueDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_detail);

        // Show the Up button in the action bar.
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Adding fragment when the activity first created.
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            VenueDetailFragment fragment
                    = VenueDetailFragment.getInstance(
                    (Venue) getIntent().getParcelableExtra(VenueDetailFragment.ARG_VENUE));
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.venue_detail_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // Use NavUtils to allow users to navigate up one level
            // in the application structure.
            NavUtils.navigateUpTo(this, new Intent(this, VenueListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
