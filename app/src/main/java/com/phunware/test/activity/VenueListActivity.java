package com.phunware.test.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


import com.phunware.test.R;
import com.phunware.test.fragment.VenueDetailFragment;
import com.phunware.test.fragment.VenueListFragment;
import com.phunware.test.model.Venue;

/**
 * An activity representing a list of Venues. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link VenueDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class VenueListActivity extends AppCompatActivity
        implements VenueListFragment.Callbacks {

    /**
     * Whether or not the activity is in two-pane mode.
     */
    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_list);

        if (findViewById(R.id.venue_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts. If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;

            // In two-pane mode, list items should be given the
            // 'activated' state when touched.
            ((VenueListFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.venue_list))
                    .setActivateOnItemClick(true);
        }

    }

    @Override
    public void onVenueSelected(Venue venue) {

        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.

            VenueDetailFragment fragment
                    = VenueDetailFragment.getInstance(venue);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.venue_detail_container, fragment)
                    .commit();

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected venue.
            Intent detailIntent = new Intent(this, VenueDetailActivity.class);
            detailIntent.putExtra(VenueDetailFragment.ARG_VENUE, venue);
            startActivity(detailIntent);
        }
    }
}
