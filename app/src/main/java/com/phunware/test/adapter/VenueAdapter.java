package com.phunware.test.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.phunware.test.R;
import com.phunware.test.model.Venue;

import java.util.List;
import java.util.Locale;

/**
 *  Adapter to handle a list of venues.
 */
public class VenueAdapter extends BaseAdapter {

    public static final int NO_POSITION_SELECTED = -1;

    private final Context mContext;
    private final List<Venue> mVenues;
    private final String mAddressFormat;
    private int mPositionSelected;

    /**
     * This adapters handles the venues view for a list.
     * @param context the actual context.
     * @param venues the list to be shown.
     */
    public VenueAdapter(Context context, List<Venue> venues) {
        mVenues = venues;
        mContext = context;
        mAddressFormat = context.getString(R.string.address_format);
        mPositionSelected = NO_POSITION_SELECTED;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        Holder holder;

        if (view == null) {

            holder = new Holder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            view = inflater.inflate(R.layout.venue_item, new LinearLayout(mContext));
            holder.mTitle = (TextView) view.findViewById(R.id.lbl_title);
            holder.mDescription = (TextView) view.findViewById(R.id.lbl_description);
            view.setTag(holder);

        } else {
            holder = (Holder) view.getTag();
        }

        Venue venue = mVenues.get(position);
        holder.mTitle.setText(venue.getName());
        holder.mDescription.setText(String.format(Locale.getDefault(), mAddressFormat
                , venue.getZip()
                , venue.getAddress()
                , venue.getCity()
                , venue.getState()));

        // Android versions previous to HoneyComb doesn't support state_activated.
        if (mPositionSelected > NO_POSITION_SELECTED) {
            if (mPositionSelected == position) {
                view.setBackgroundColor(mContext.getResources().getColor(R.color.blue));
            } else {
                view.setBackgroundColor(Color.TRANSPARENT);
            }
        }

        return view;
    }

    @Override
    public Object getItem(int position) {
        return mVenues.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getCount() {
        return mVenues.size();
    }


    public static class Holder {
        public TextView mTitle, mDescription;
    }

    /**
     * Set the position selected in the list.
     * This should be called in Android versions previous to HoneyComb.
     * @param positionSelected the position in the list to be selected.
     */
    public void setPositionSelected(int positionSelected) {
        this.mPositionSelected = positionSelected;
        notifyDataSetChanged();
    }
}
