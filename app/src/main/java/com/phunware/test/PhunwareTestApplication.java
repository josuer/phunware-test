package com.phunware.test;

import android.app.Application;

import com.phunware.test.api.ApiClient;
import com.phunware.test.util.DateFormatUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public class PhunwareTestApplication extends Application {

    /**
     * The server url.
     */
    private static final String SERVER = "https://s3.amazonaws.com/jon-hancock-phunware";

    private ApiClient mApi;

    @Override
    public void onCreate() {
        super.onCreate();


        //Initializing the rest adapter and setting up the
        //date used in the models.
        OkHttpClient client = new OkHttpClient();

        Gson gson = new GsonBuilder()
                .setDateFormat(DateFormatUtil.DATE_FORMAT)
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setClient(new OkClient(client))
                .setEndpoint(SERVER)
                .setConverter(new GsonConverter(gson))
                .build();

        restAdapter.setLogLevel(RestAdapter.LogLevel.BASIC);

        mApi = new ApiClient(restAdapter);
    }

    /**
     *  Return the API for server requests.
     * @return Api Client
     */
    public ApiClient getApi() {
        return mApi;
    }
}
