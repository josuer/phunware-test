package com.phunware.test.fragment;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.phunware.test.PhunwareTestApplication;
import com.phunware.test.R;
import com.phunware.test.adapter.VenueAdapter;
import com.phunware.test.model.Venue;
import com.phunware.test.util.NetworkUtil;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A list fragment representing a list of Venues.
 */
public class VenueListFragment extends ListFragment {

    /**
     * The Bundle key representing the activated item position.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";

    /**
     * The fragment's current callback object.
     */
    private Callbacks mCallbacks = sVenueCallbacks;

    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = ListView.INVALID_POSITION;

    /**
     * Venues adapter.
     */
    private VenueAdapter mAdapter;

    /**
     * Interface to communicate with it's parent.
     */
    public interface Callbacks {
        /**
         * This method should be called when the user taps a venue.
         * @param venue selected by the user.
         */
        void onVenueSelected(Venue venue);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sVenueCallbacks = new Callbacks() {
        @Override
        public void onVenueSelected(Venue venue) {
        }
    };

    /**
     * Mandatory empty constructor.
     */
    public VenueListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (NetworkUtil.isNetworkAvailable(getActivity())) {
            //Downloading the venues.
            ((PhunwareTestApplication) getActivity().getApplication()).getApi().getVenues()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<List<Venue>>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onNext(List<Venue> venues) {
                            // Populating the venues list.
                            mAdapter = new VenueAdapter(getActivity(), venues);
                            setListAdapter(mAdapter);
                        }
                    });
        } else {
            Toast.makeText(getActivity()
                    , R.string.network_not_available
                    , Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Restore the previously serialized activated item position.
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }

        //removing the list separators
        getListView().setDivider(null);
        getListView().setDividerHeight(0);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sVenueCallbacks;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        // Notify the active callbacks interface that an item has been selected.
        mCallbacks.onVenueSelected((Venue) listView.getItemAtPosition(position));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    /**
     * When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;

        // Android versions previous to HoneyComb doesn't support state_activated.
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB
                && mAdapter != null){
            mAdapter.setPositionSelected(mActivatedPosition);
        }
    }
}
