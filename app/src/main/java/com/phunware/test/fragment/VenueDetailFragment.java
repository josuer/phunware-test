package com.phunware.test.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.phunware.test.R;

import com.phunware.test.model.ScheduleItem;
import com.phunware.test.model.Venue;
import com.phunware.test.util.DateFormatUtil;
import com.squareup.picasso.Picasso;

/**
 * A fragment representing a single Venue detail screen.
 */
public class VenueDetailFragment extends Fragment {

    /**
     * The fragment argument representing the venue that this fragment
     * represents.
     */
    public static final String ARG_VENUE = "VENUE";

    /**
     * The venue content this fragment is presenting.
     */
    private Venue mVenue;

    /**
     * Mandatory empty constructor.
     */
    public VenueDetailFragment() {
    }


    /**
     * Return an instance of the fragment with the required args.
     * @param venue to send as an argo to the fragment.
     * @return VenueDetailFragment with it's arguments set.
     */
    public static VenueDetailFragment getInstance(Venue venue) {
        Bundle arguments = new Bundle();
        arguments.putParcelable(VenueDetailFragment.ARG_VENUE,venue);
        VenueDetailFragment fragment = new VenueDetailFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments().containsKey(ARG_VENUE)) {
            // Load the venue content specified by the fragment
            // arguments
            mVenue = getArguments().getParcelable(ARG_VENUE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_detail, menu);

        MenuItem item = menu.findItem(R.id.action_share);
        ShareActionProvider shareActionProvider = (ShareActionProvider)
                MenuItemCompat.getActionProvider(item);

        //This should update share intent.
        Intent shareIntent = new Intent(Intent.ACTION_SEND);

        shareIntent.putExtra(Intent.EXTRA_TEXT
                , getString(R.string.full_place_format
                , mVenue.getName()
                , mVenue.getAddress()
                , mVenue.getCity()
                , mVenue.getState()));

        shareIntent.setType("text/plain");

        shareActionProvider.setShareIntent(shareIntent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);

        // Show venue data in views.
        if (mVenue != null) {
            ImageView image = (ImageView) rootView.findViewById(R.id.img_stadium);
            TextView name = (TextView) rootView.findViewById(R.id.lbl_name);
            TextView address = (TextView) rootView.findViewById(R.id.lbl_address);
            TextView address2 = (TextView) rootView.findViewById(R.id.lbl_address_2);
            TextView schedules = (TextView) rootView.findViewById(R.id.lbl_schedules);
            View dataContainer = rootView.findViewById(R.id.container);

            //This is just for tablets to show the card view
            if (dataContainer != null && dataContainer.getVisibility() == View.GONE) {
                dataContainer.setVisibility(View.VISIBLE);
            }

            if (mVenue.getImageUrl() == null || mVenue.getImageUrl().isEmpty()) {
                Picasso.with(getActivity()).load(R.drawable.ic_not_found).into(image);
            } else {
                Picasso.with(getActivity()).load(mVenue.getImageUrl())
                        .into(image);
            }

            name.setText(mVenue.getName());
            address.setText(mVenue.getAddress());

            address2.setText(getString(R.string.city_and_state_format
                    , mVenue.getCity(), mVenue.getState()));

            StringBuilder builder = new StringBuilder();
            for(ScheduleItem scheduleItem : mVenue.getSchedule()) {
                builder.append(getString(R.string.schedule_format
                        , DateFormatUtil.getPrettyStartDate(
                        scheduleItem.getStartDate())
                        , DateFormatUtil.getPrettyEndDate(
                        scheduleItem.getEndDate())));
            }
            schedules.setText(builder.toString());
        }

        return rootView;
    }

}
