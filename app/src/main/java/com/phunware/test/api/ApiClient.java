package com.phunware.test.api;

import com.phunware.test.model.Venue;

import java.util.List;

import retrofit.RestAdapter;
import rx.Observable;

/**
 *  This class gives access to all the api calls.
 *  This class is uses an implementation of the ApiService class.
 */
public class ApiClient {
    private final ApiService mApiService;

    /**
     * Class that handles api calls.
     * @param restAdapter rest adapter that will
     *                    handle the api calls.
     */
    public ApiClient(RestAdapter restAdapter) {
        mApiService = restAdapter.create(ApiService.class);
    }

    /**
     * Get the list of Venues.
     * @return List of venues.
     */
    public Observable<List<Venue>> getVenues() {
        return mApiService.getVenues();
    }

}
