package com.phunware.test.api;

import com.phunware.test.model.Venue;

import java.util.List;

import retrofit.http.GET;
import rx.Observable;

/**
 * This class represents all the definitions of the API.
 */
public interface ApiService {

    /**
     * Retrieves the venues from server.
     * @return The list of venues.
     */
    @GET("/nflapi-static.json")
    Observable<List<Venue>> getVenues();

}