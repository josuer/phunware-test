package com.phunware.test.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Venue implements Parcelable {

    // Core fields
    @SerializedName("id") private long mId;
    @SerializedName("pcode") private int mPcode;
    @SerializedName("latitude") private double mLatitude;
    @SerializedName("longitude") private double mLongitude;
    @SerializedName("name") private String mName;
    @SerializedName("address") private String mAddress;
    @SerializedName("city") private String mCity;
    @SerializedName("state") private String mState;
    @SerializedName("zip") private String mZip;
    @SerializedName("phone") private String mPhone;

    // Super Bowl venue fields
    @SerializedName("tollfreephone") private String mTollFreePhone;
    @SerializedName("url") private String mUrl;
    @SerializedName("description") private String mDescription;
    @SerializedName("ticket_link") private String mTicketLink;
    @SerializedName("image_url") private String mImageUrl;
    @SerializedName("schedule") private ArrayList<ScheduleItem> mSchedule;

    // computed fields
    private float mDistance;

    private long getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getZip() {
        return mZip;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getCity() {
        return mCity;
    }

    public String getState() {
        return mState;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Venue && ((Venue) o).getId() == mId;
    }

    @Override
    public int hashCode() {
        return Long.valueOf(mId).hashCode();
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public ArrayList<ScheduleItem> getSchedule() {
        return mSchedule;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.mId);
        dest.writeInt(this.mPcode);
        dest.writeDouble(this.mLatitude);
        dest.writeDouble(this.mLongitude);
        dest.writeString(this.mName);
        dest.writeString(this.mAddress);
        dest.writeString(this.mCity);
        dest.writeString(this.mState);
        dest.writeString(this.mZip);
        dest.writeString(this.mPhone);
        dest.writeString(this.mTollFreePhone);
        dest.writeString(this.mUrl);
        dest.writeString(this.mDescription);
        dest.writeString(this.mTicketLink);
        dest.writeString(this.mImageUrl);
        dest.writeList(this.mSchedule);
        dest.writeFloat(this.mDistance);
    }

    private Venue(Parcel in) {
        this.mId = in.readLong();
        this.mPcode = in.readInt();
        this.mLatitude = in.readDouble();
        this.mLongitude = in.readDouble();
        this.mName = in.readString();
        this.mAddress = in.readString();
        this.mCity = in.readString();
        this.mState = in.readString();
        this.mZip = in.readString();
        this.mPhone = in.readString();
        this.mTollFreePhone = in.readString();
        this.mUrl = in.readString();
        this.mDescription = in.readString();
        this.mTicketLink = in.readString();
        this.mImageUrl = in.readString();
        this.mSchedule = new ArrayList<>();
        in.readList(this.mSchedule, ScheduleItem.class.getClassLoader());
        this.mDistance = in.readFloat();
    }

    public static final Creator<Venue> CREATOR = new Creator<Venue>() {
        public Venue createFromParcel(Parcel source) {
            return new Venue(source);
        }

        public Venue[] newArray(int size) {
            return new Venue[size];
        }
    };
}