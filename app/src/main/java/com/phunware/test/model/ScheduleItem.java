package com.phunware.test.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ScheduleItem implements Parcelable {

    @SerializedName("start_date") private Date mStartDate;
    @SerializedName("end_date") private Date mEndDate;

    public Date getStartDate() {
        return mStartDate;
    }

    public Date getEndDate() {
        return mEndDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mStartDate != null ? mStartDate.getTime() : -1);
        dest.writeLong(mEndDate != null ? mEndDate.getTime() : -1);
    }

    private ScheduleItem(Parcel in) {
        long tmpMStartDate = in.readLong();
        this.mStartDate = tmpMStartDate == -1 ? null : new Date(tmpMStartDate);
        long tmpMEndDate = in.readLong();
        this.mEndDate = tmpMEndDate == -1 ? null : new Date(tmpMEndDate);
    }

    public static final Creator<ScheduleItem> CREATOR = new Creator<ScheduleItem>() {
        public ScheduleItem createFromParcel(Parcel source) {
            return new ScheduleItem(source);
        }

        public ScheduleItem[] newArray(int size) {
            return new ScheduleItem[size];
        }
    };
}