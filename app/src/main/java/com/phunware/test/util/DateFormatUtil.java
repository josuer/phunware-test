package com.phunware.test.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * This class consists exclusively of static methods that return formatted dates.
 */
public class DateFormatUtil {
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss Z";
    private static final String DATE_FORMAT_START_DATE = "EEEE M/d HH:mm";
    private static final String DATE_FORMAT_END_DATE = "HH:mm";

    private static final SimpleDateFormat sStartDateFormatter = new SimpleDateFormat(
            DATE_FORMAT_START_DATE, Locale.US);
    private static final SimpleDateFormat sEndDateFormatter = new SimpleDateFormat(
            DATE_FORMAT_END_DATE, Locale.US);

    /**
     * This method return a formatted date.
     * @param startDate the date to be formatted.
     * @return formatted date string in the form EEEE M/d HH:mm.
     */
    public static String getPrettyStartDate(Date startDate) {
        return sStartDateFormatter.format(startDate);
    }

    /**
     * This method return a formatted date.
     * @param endDate the date to be formatted.
     * @return formatted date string in the form HH:mm.
     */
    public static String getPrettyEndDate(Date endDate) {
        return sEndDateFormatter.format(endDate);
    }
}
