package com.phunware.test.util;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * This class consists exclusively of static methods that help with network states.
 */
public class NetworkUtil {

    /**
     * Check if a network connection exist.
     * @param context the current context.
     * @return True if network is available.
     */
    public static boolean isNetworkAvailable(Context context) {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE))
                .getActiveNetworkInfo() != null;
    }
}
